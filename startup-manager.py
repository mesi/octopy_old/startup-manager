import os
import json
import logging
import subprocess
import signal
import sys
import re
from time import sleep
from lib.config_loader import config_loader


APP_ID = 'startup manager'
LOG_LEVEL = logging.INFO

#for module in config['modules']:
#    mod_path = os.path.join(config['path'], module)
#    cmd = os.path.join(mod_path, START_SCRIPT)
#    if os.path.exists(cmd):
#        os.chdir(mod_path)
#        logging.info(f"Starting module: {cmd}")
#        #os.system(cmd)

# Setup logging
sh = logging.StreamHandler(stream = sys.stdout)
formatter = logging.Formatter('%(levelname)s:%(message)s')
sh.setFormatter(formatter)
sh.setLevel(LOG_LEVEL)
logger = logging.getLogger(APP_ID)
logger.setLevel(logging.DEBUG)
logger.addHandler(sh)
logger.info("Starting...")

# Load configuration file
config = config_loader.load()
client_id = f"{config['system id']}:{APP_ID}"

processes = {}


# Stops running processes and exit the program
def stop(reason):
    if (reason):
        logging.error(f"Terminating Octopy: {reason}")
    else:
        logging.error("Terminating Octopy")
    global processes
    # Kill all running sub processes
    for proc_name in processes:
        process = processes[proc_name]
        os.killpg(os.getpgid(process.pid), signal.SIGTERM)
    sys.exit(0)


try:
    while True:
        # A bit of a temporary hack - reload config every time to monitor for changes
        try:
            new_config = config_loader.load()
        except Exception as e:
            logging.error(f"Could not load config: {e}")
        config = new_config
        logging.info("Checking for running processes")
        for module in config['startup']['modules']:
            mod_name = module['name']
            if module.get('disable') or module.get('disabled'):
                if (mod_name in processes and processes[mod_name].poll() is None):
                    # Module is running, but has been disabled - kill it
                    processes[mod_name].kill()
            elif not (mod_name in processes) or not (processes[mod_name].poll() is None):
                if processes.get(mod_name):
                    print(processes.get(mod_name).poll())
                    # Process is not running
                logging.warning(f"\"{mod_name}\" is not running - starting...")
                mod_path = '.'
                if 'path' in module:
                    mod_path = os.path.abspath(os.path.join(config['startup']['path'], module['path']))
                    process = None
                if module.get('screen'):
                    screen_name = re.sub('[\s:\?\'\"]', '_', module['name'])
                    screen_cmd = ['screen', '-Dm', '-S', screen_name]
                    processes[mod_name] = subprocess.Popen(screen_cmd, cwd = mod_path, preexec_fn = os.setsid)
                    sleep(0.5)
                    screen_cmd = ['screen', '-S', screen_name, '-X', 'stuff', f"{module['command']}\n"]
                    subprocess.Popen(screen_cmd, cwd = mod_path)
                else:
                    args = [ module['command'] ]
                    processes[mod_name] = subprocess.Popen(args, cwd = mod_path, preexec_fn = os.setsid)
                if not processes[mod_name].pid:
                    logging.error("Module \"{mod_name}\" did not start")
                else:
                    logging.info(f"Started \"{mod_name}\", pid={processes[mod_name].pid}, command=\"{module['command']}\"")
        sleep(config['startup']['update interval'])

except BaseException as err:
    stop(str(err))
